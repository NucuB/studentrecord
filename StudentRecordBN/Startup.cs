﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentRecordBN.ESender;
using StudentRecordBN.Models;
using StudentRecordBN.Repository;

namespace StudentRecordBN
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var connection = @"Server=DESKTOP-3FJQEGH\MSSQLSERVER01;Database=BaleaNDB;Trusted_Connection=True;ConnectRetryCount=0";
            services.AddDbContext<AppDBContext>(options => options.UseSqlServer(connection));
            //services.AddDbContext<RepositoryContext>(options => options.UseSqlServer(connection));
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            
            //services.ConfigureMySqlContext(Configuration);???????????????????
            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient<IStudent, Student>();
            services.AddTransient(typeof(IStudent), typeof(Student));

            services.AddTransient<IProject, Project>();
            services.AddTransient(typeof(IProject), typeof(Project));
            services.AddTransient<IGrade, Grade>();
            services.AddTransient(typeof(IGrade), typeof(Grade));
            services.AddTransient<ITeacher, Teacher>();
            services.AddTransient(typeof(ITeacher), typeof(Teacher));
            // services.AddTransient<IEMailSender, EMailSender>();

            //EMAIL CONFIRM
           /* services.AddDefaultIdentity<IdentityUser>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;
            });*/
            services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<AppDBContext>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = true;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                options.User.RequireUniqueEmail = true;
                //options.User.AllowedUserNameCharacters;

            });
            
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays(150);
                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
                options.Events = new Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationEvents
                {
                    OnRedirectToLogin = ctx =>
                    {
                        var requestPath = ctx.Request.Path;
                        if (requestPath.Value == "/Students/Index")
                        {
                            ctx.Response.Redirect("/Identity/Account/AccessDenied");
                        }


                        return Task.CompletedTask;
                    }
                };

            });
            //EXTERNAL LOG IN
            /*services.AddAuthentication()
                .AddMicrosoftAccount(microsoftOptions => { })
                .AddGoogle(googleOptions => {  })
                .AddTwitter(twitterOptions => { })
                .AddFacebook(facebookOptions => { });*/

            /* services.Configure<IdentityOptions>(config =>
             {
                 config.Cookies.ApplicationCookie.Events =
                 new CookieAuthenticationEvents
                 {
                     OnRedirectToLogin = ctx =>
                     {
                         if (ctx.Request.Path.StartsWithSegments("/api") && ctx.Response.StatusCode == 200)
                         {
                             ctx.Response.StatusCode = 401;
                             return Task.FromResult<object>(null);
                         }

                         ctx.Response.Redirect(ctx.RedirectUri);
                         return Task.FromResult<object>(null);
                     }
                 };
             });*/


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();



            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
