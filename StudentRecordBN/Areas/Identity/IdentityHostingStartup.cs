﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StudentRecordBN.Models;

[assembly: HostingStartup(typeof(StudentRecordBN.Areas.Identity.IdentityHostingStartup))]
namespace StudentRecordBN.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<AppDBContextContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("AppDBContextContextConnection")));

               // services.AddDefaultIdentity<IdentityUser>()
                   // .AddEntityFrameworkStores<AppDBContextContext>();
            });
        }
    }
}