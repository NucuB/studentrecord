﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class Project : IProject
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string Subject { get; set; }
        public int TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public Project(int Id, string ProjectName, string Subject)
        {
            this.ProjectId = ProjectId;
            this.ProjectName = ProjectName;
            this.Subject = Subject;
        }

        public Project()
        {

        }

        public string GetDetails()
        {
            throw new NotImplementedException();
        }
    }
}
