﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class Grade : IGrade
    {
        [Key]
        public int GradeId { get; set; }

        public string Subject { get; set; }
        public int Value { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }

        public Grade(int GradeId, string Subject, int Value)
        {
            this.GradeId = GradeId;
            this.Subject = Subject;
            this.Value = Value;
        }

        public Grade()
        {

        }

        public bool IsEditable()
        {
            throw new NotImplementedException();
        }

        public bool IsGraduateGrade()
        {
            throw new NotImplementedException();
        }
    }
}
