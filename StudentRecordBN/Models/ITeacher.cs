﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    interface ITeacher
    {
        void AssignMark(Grade Grade);
        void EditMark(Grade Grade);
        void PutComment(String Comment);
        void EditComment(String CommentToBeEdited, String NewComment);
        IEnumerable<Student> GetAllEnrolledStudents();
    }
}
