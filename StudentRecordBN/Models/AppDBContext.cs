﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class AppDBContext : IdentityDbContext<IdentityUser>
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }


        public DbSet<Student> Students { get; set; }
        public DbSet<StudentAddress> StudentsAddresses { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TeacherAddress> TeachersAddresses { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Grade> Grades { get; set; }

        public static implicit operator AppDBContext(HostingApplication.Context v)
        {
            throw new NotImplementedException();
        }

        // protected override void OnModelCreating(ModelBuilder modelBuilder) => modelBuilder.Entity<Project>()
        //  .HasKey(c => c.ProjectId);


    }
}
