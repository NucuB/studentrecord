﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    interface IGrade
    {
        bool IsEditable();
        bool IsGraduateGrade();
    }
}
