﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class Student : IStudent
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }

        public StudentAddress Address { get; set; }

        public virtual ICollection<Grade> Grades { get; set; }


        public Student(int Id, string Name, int Age, StudentAddress Address)
        {
            this.StudentId = Id;
            this.StudentName = Name;
            this.Age = Age;
            this.Address = Address;

        }

        public Student(int Id, string Name, int Age)
        {
            this.StudentId = Id;
            this.StudentName = Name;
            this.Age = Age;
            this.Address = null;

        }

        public Student() { }

        public bool Login()
        {
            throw new NotImplementedException();
        }

        public void Register()
        {
            throw new NotImplementedException();
        }

        public void ChangePassword()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Grade> GetGrades()
        {
            throw new NotImplementedException();
        }



        public IEnumerable<Project> GetAvailableProjects()
        {
            throw new NotImplementedException();
        }

        public bool CheckGrantStatus()
        {
            throw new NotImplementedException();
        }
    }

    public partial class StudentAddress
    {
        [Key]
        public int StudentId { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public Student Student { get; set; }

        public StudentAddress(int StudentId, string Address, string City, Student Student)
        {
            this.StudentId = StudentId;
            this.Address = Address;
            this.City = City;
            this.Student = Student;
        }

        public StudentAddress()
        {

        }
    }
}
