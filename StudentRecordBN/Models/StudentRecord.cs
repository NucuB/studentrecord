﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class StudentRecord
    {
        public int StudentRecordId;
        public ICollection<Student> StudentList;
        public ICollection<Teacher> TeacherList;

        public StudentRecord(int Id)
        {
            this.StudentRecordId = Id;
            StudentList = new List<Student>();
            TeacherList = new List<Teacher>();
        }

        public StudentRecord()
        {

        }
    }
}
