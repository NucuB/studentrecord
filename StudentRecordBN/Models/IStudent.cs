﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    interface IStudent
    {
        bool Login();
        void Register();
        void ChangePassword();
        IEnumerable<Grade> GetGrades();
        Boolean CheckGrantStatus();
        IEnumerable<Project> GetAvailableProjects();
    }
}
