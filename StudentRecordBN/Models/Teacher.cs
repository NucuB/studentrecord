﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Models
{
    public class Teacher : ITeacher
    {
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int Age { get; set; }
        public TeacherAddress Address { get; set; }

        [NotMapped]
        public ICollection<String> SubjectsTeachedList { get; set; }

        [NotMapped]
        public ICollection<Project> ProjectsList { get; set; }

        public Teacher(int TeacherId, string TeacherName, int Age, TeacherAddress TeacherAddress)
        {
            this.TeacherId = TeacherId;
            this.TeacherName = TeacherName;
            this.Age = Age;
            this.Address = TeacherAddress;
            this.SubjectsTeachedList = new List<String>();
            this.ProjectsList = new List<Project>();

        }
        public Teacher(int TeacherId, string TeacherName, int Age)
        {
            this.TeacherId = TeacherId;
            this.TeacherName = TeacherName;
            this.Age = Age;
            this.Address = null;
            this.SubjectsTeachedList = new List<String>();
            this.ProjectsList = new List<Project>();
        }
        public Teacher()
        {

        }

        public void AssignMark(Grade Grade)
        {
            throw new NotImplementedException();
        }

        public void PutComment(string Comment)
        {
            throw new NotImplementedException();
        }

        public void EditComment(string CommentToBeEdited, string NewComment)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Student> GetAllEnrolledStudents()
        {
            throw new NotImplementedException();
        }

        public void EditMark(Grade Grade)
        {
            throw new NotImplementedException();
        }
    }

    public partial class TeacherAddress
    {
        [Key]
        public int TeacherId { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public Teacher Teacher { get; set; }


        public TeacherAddress(int TeacherId, string Address, string City, Teacher Teacher)
        {
            this.TeacherId = TeacherId;
            this.Address = Address;
            this.City = City;
            this.Teacher = Teacher;
        }

        public TeacherAddress()
        {

        }
    }
}
