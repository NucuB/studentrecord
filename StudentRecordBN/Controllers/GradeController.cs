﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentRecordBN.Repository;

namespace StudentRecordBN.Controllers
{
    [Route("api/[controller]")]
    public class GradeController : Controller
    {
        private IRepositoryWrapper _repoWrapper;

        public GradeController(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            var students = _repoWrapper.Grade.FindByCondition(x => x.Subject.Equals("Informatica"));
            var owners = _repoWrapper.Grade.FindAll();

            return new string[] { "value1", "value2" };
        }
    }
}