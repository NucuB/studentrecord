﻿using StudentRecordBN.Models;
using StudentRecordBN.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentRecordBN.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private AppDBContext _repoContext;

        private ITeacherRepository _teacher;
        private IStudentRepository _student;
        private IProjectRepository _project;
        private IGradeRepository _grade;


        public ITeacherRepository Teacher
        {
            get
            {
                if (_teacher== null)
                {
                    _teacher = new TeacherRepository(_repoContext);
                }

                return _teacher;
            }
        }


        public IStudentRepository Student
        {
            get
            {
                if (_student == null)
                {
                    _student = new StudentRepository(_repoContext);
                }

                return _student;
            }
        }
        public IProjectRepository Project
        {
            get
            {
                if (_project == null)
                {
                    _project = new ProjectRepository(_repoContext);
                }

                return _project;
            }
        }
        public IGradeRepository Grade
        {
            get
            {

                if (_grade == null)
                {
                    _grade= new GradeRepository(_repoContext);
                }

                return _grade;
            }
        }


        public RepositoryWrapper(AppDBContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
    }
}
