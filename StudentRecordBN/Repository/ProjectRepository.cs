﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentRecordBN.Models;

namespace StudentRecordBN.Repository
{
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(AppDBContext dbContext)
                : base(dbContext)
        {

        }
    }
}
