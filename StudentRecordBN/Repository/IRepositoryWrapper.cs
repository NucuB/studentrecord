﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentRecordBN.Models;

namespace StudentRecordBN.Repository
{
    public interface IRepositoryWrapper
    {
        ITeacherRepository Teacher{ get; }
        IStudentRepository Student { get; }
        IProjectRepository Project { get; }
        IGradeRepository Grade { get; }
        
    }
}
