﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentRecordBN.Models;

namespace StudentRecordBN.Repository
{
    public class StudentRepository : RepositoryBase<Student>, IStudentRepository
    {
        public StudentRepository(AppDBContext dbContext)
                : base(dbContext)
        {

        }
    }
}
