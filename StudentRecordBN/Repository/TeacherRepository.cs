﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentRecordBN.Models;

namespace StudentRecordBN.Repository
{
    public class TeacherRepository : RepositoryBase<Teacher>, ITeacherRepository
    {
        public TeacherRepository(AppDBContext dbContext)
                : base(dbContext)
        {

        }
    }
}
