# README #

## Scope

This application should provide a friendly enviroment for managing students classes, grades, projects.



### Functionalities ###

* Register/Login(Roles:Teacher/Student)
* Teacher panel(CRUD)
* Student-User panel(CRUD with authorize system)

### Development 

* Developed in C#, ASP.NET CORE
* Database: Microsoft SQL Server Management(Code-first approach)